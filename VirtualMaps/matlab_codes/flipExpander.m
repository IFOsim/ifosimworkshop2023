% function [mapOut, xOut, yOut] = flipExpander( mapIn, xIn, yIn, r0, rOut, args )
% newMap( r, theta) = oldMap( 2r0 - r, theta ) * (1 + (r-r0)/r0*amp)
% When zernikeN is given, oldMap is the original map without zernike polynominal up to zesrnikeN (3:powe, 5:Astigm)
%
% size(map) should be [length(y), length(x)]
%
% March 2, 2023
%    Default values for args.
%
% October 31, 2022
%    arguments changed to support new format for map[extID, args]
%    default arguments are amp = 3, zermikeN = 3
%
% September 7, 2022
%    improvement of smoothness
%
% September 19, 2020
%    zernikeCoef used radius for aperture. Fixed
%
% June 20, 2020
%    Zernike term is added
%
% October 27, 2015  update with newX and newY as return values
%
% Nov. 20, 2016  BROKEN
%    map( xg.^2 + yg.^2 <= r0^2 ) oversized
%

function [mapOut, xOut, yOut] = flipExpander( mapIn, xIn, yIn, r0, rOut, args )

% set defaults
% amplification outside, amp = 0 no amplification
if length(args) < 1
    amp = 3;
else
    amp = args(1);
end
% zernike fit of internal data for expansion
if length(args) < 2
    zernikeN = 3;
else
    zernikeN = args(2);
end

dx = xIn(2)-xIn(1); dy = yIn(2)-yIn(1);

% new (xlist,ylist), same bin size, cover new area up to rOut
new0 = xIn(1) - ceil( (rOut + xIn(1)) / dx )*dx;
nx = floor( ( xIn(end) + ceil( (rOut - xIn(end)) / dx )*dx - new0 ) / dx ) + 1;
xOut = (0:nx-1)*dx + new0;
new0 = yIn(1) - ceil( (rOut + yIn(1)) / dy )*dy;
ny = floor( ( yIn(end) + ceil( (rOut - yIn(end)) / dy )*dy - new0 ) / dy ) + 1;
yOut = (0:ny-1)*dy + new0;

% new data size is (ny, nx)
mapOut = nan( ny, nx );
[xg,yg] = meshgrid( xIn, yIn );
[newXg, newYg] = meshgrid( xOut, yOut );
newRg2 = newXg.^2 + newYg.^2;
newRg = sqrt(newRg2);
% copy old data in mapIn to new map mapOut
mapOut( newRg2 <= r0^2 ) = mapIn( xg.^2 + yg.^2 <= r0^2 );

% if zernikeN is given, remve those zernite terms from mapIn
if exist( 'zernikeN', 'var' )
    [mapIn, coef, nmList] = zernikeCoef( mapIn, xIn, yIn, 2*r0, zernikeN );
    nmList(:,3) = coef;
end

ind = find( (newRg2 > r0^2) & (newRg2 <= rOut^2) );
% out side data (r, theta) = old data(2*r0 - r(new), theta)
theta = atan2( newYg(ind), newXg(ind) );
rrev = abs(2*r0 - newRg(ind));

% expand by 2*r0 - r to cover all area
while( max(rrev) > r0 )
    jnd = find( rrev > r0 );
    rrev(jnd) = abs(2*r0 - rrev(jnd));
end
% (xgrev,ygrev) covers all expanded area using original data - zernike terms
xgrev = floor( rrev.*cos(theta) / dx + size(mapIn,2)/2 + 0.5) + 1;
ygrev = floor( rrev.*sin(theta) / dy + size(mapIn,1)/2 + 0.5) + 1;
for n = 1 : length(ind)
    id = ind(n);
    mapOut(id) = mapIn( ygrev(n), xgrev(n) ) .* (1 + (newRg(id)/r0-1)*amp);
end
% if zernikeN is given, original map area has no zernike terms, 
% so add it up to restore the original shape
if exist( 'zernikeN', 'var' )
    zerSum = zernikeSum( xOut, yOut, nmList, r0, rOut );
    mapOut(ind) = mapOut(ind) + zerSum(ind) .* (1 + (newRg(ind)/r0-1)*amp);
end
