function [mapOut, xOut, yOut] = mapDoubler( mapIn, xIn, yIn, r0, shape ) %, zernikeN )
% function [mapX, xOutX, ylX] = mapDoubler( map, xl, yl, A, shape ) %, zernikeN )
% Convert data given map defined at (xl,yl) to frequency doamin using data in aperture
% and expand the low frequency region to 1/(2*A), and convert back to x domain
% (0~N/2-1)+(-N/2~-1) => (-N/2~N/2-1) => (-N~N-1) => (0~N-1)+(-N~-1)
% if shape is 'circle', data in aperture A is used to expand to a
% circle with diameter of 2xA
% otherwise, data in (A x A) are used to generate data in (2A x 2A)
%
% June 20, 2020
%   Zernike term manipulation added, but does not work
%

% Remove the NaN if any
mapIn(isnan(mapIn)) = 0;

% make nx = ny, so dx and dy should be similar
dx = xIn(2) - xIn(1);
dy = yIn(2) - yIn(1);

if abs(dx/dy-1) > 0.001
    fprintf(1, 'x and y pixel sizes are different by %f, be warned\n', dx/dy-1);
end

% x range
indx = find( abs(xIn) <= r0 );
if mod(length(indx),2) ~= 0
    indx = indx(1:end-1);
end
indy = find( abs(yIn) <= r0 );
if mod(length(indy),2) ~= 0
    indy = indy(1:end-1);
end

dleng = length(indx) - length(indy);
if dleng > 0
    off = ceil( dleng/2 );
    indx = indx(off:off+length(indy)-1);
elseif dleng < 0
    off = ceil( -dleng/2 );
    indy = indy(off:off+length(indx)-1);
end

N = length(indx);

map = mapIn(indy,indx);

% zernike subtraction
% if exist( 'zernikeN', 'var' )
%    [map, coef, nmList] = zernikeCoef( map, x(indx), y(indy), A/2, zernikeN );
%    nmList(:,3) = coef;
% end

% default shape is circular
%{
if ~exist('shape', 'var') || shape == 'c'
    w = hanning(N);
    m = w(:)*w(:).'; % Outer product window
    
    P_window =  (sum(sum(abs(m).^2))/N^2); % Power of the window
    
    map = map.*m/sqrt(P_window)/2;
end
%}

ASD_2D = fftshift(fft2(ifftshift(map)));

rng2 = 2:2:N*2;

ASD_2D_L = zeros( N*2+2, N*2+2 );

ASD_2D_L( rng2+1, rng2 ) = ASD_2D/2;
ASD_2D_L( 1, rng2 ) = ASD_2D(end,:)/2;
ASD_2D_L( rng2-1, rng2 ) = ASD_2D_L( rng2-1, rng2 ) + ASD_2D/2;
ASD_2D_L( rng2, rng2+1 ) = ASD_2D/2;
ASD_2D_L( rng2, 1 ) = ASD_2D(:,end)/2;
ASD_2D_L( rng2, rng2-1 ) = ASD_2D_L( rng2, rng2-1 ) + ASD_2D/2;

ASD_2D_L( rng2+1, rng2+1 ) = ASD_2D_L( rng2+1, rng2   )/4;
ASD_2D_L( rng2+1, rng2+1 ) = ASD_2D_L( rng2+1, rng2+2 )/4 + ASD_2D_L( rng2+1, rng2+1 );
ASD_2D_L( rng2+1, rng2+1 ) = ASD_2D_L( rng2,   rng2+1 )/4 + ASD_2D_L( rng2+1, rng2+1 );
ASD_2D_L( rng2+1, rng2+1 ) = ASD_2D_L( rng2+2, rng2+1 )/4 + ASD_2D_L( rng2+1, rng2+1 );

ASD_2D_L( rng2, rng2 ) = ASD_2D;

ASD_2D_L = ifftshift( ASD_2D_L(2:end-1,2:end-1) );
ASD_2D_L(2:N,1) = (ASD_2D_L(2:N,1) + conj(ASD_2D_L(2*N:-1:N+2,1))) / 2;
ASD_2D_L(2*N:-1:N+2,1) = conj( ASD_2D_L(2:N,1) );
ASD_2D_L(1,1) = real( ASD_2D_L(1,1) );
ASD_2D_L(N+1,1) = real( ASD_2D_L(N+1,1) );

mapOut = fftshift( ifft2( ASD_2D_L ) );

mapOut = real( mapOut );

% make xOut overlaps with input coordinates
indmin = find( xIn == min(abs(xIn)), 1 );
xOut = ((1:2*N)-N)*dx + xIn(indmin); % xOut(N) = x(indmin)
yOut = xOut;
% if exist( 'zernikeN', 'var' )
%    mapOut = mapOut + zernikeSum( xOut, xOut, nmList, A/2, A );
% end

% fill the original data in the center
% somehow this does not work
% mapOut( UtilTK.Rsquared(xOut, yOut) <= r0^2 ) = mapIn( UtilTK.Rsquared( xIn, yIn ) <= r0^2 );
% for ix = 1 : length(xIn)
%    ind = find( mapIn(:,ix) ~= 0 );
%    if ~isempty(ind)
%        mapOut( floor(N-length(yIn)/2) + ind,  floor(N-length(xIn)/2) + ix) = mapIn( ind, ix );
%    end
% end

