import numpy as np
from scipy.special import factorial

def findBoundary(data, xIn, yIn, notZero):
    if notZero:
        ind = np.logical_and(np.isfinite(data), data != 0)
    else:
        ind = data != 0
    if np.min(xIn.shape) == 1:
        xIn, yIn = np.meshgrid(xIn, yIn)
    return np.min(xIn[ind]), np.max(xIn[ind]), np.min(yIn[ind]), np.max(yIn[ind])

# zernike function from zernike_basis().ipynb
# maybe replace with import from other file?
# return a list of Zernike maps(basis) 
def Zernikes(shape, radius, max_zern): 
    '''
    shape = int, number of points of the real map that describe the mirror, like np.shape(x-axis). 
    radius = int, the radius will be the real mirror radius.  
    max_zern = int, the basis will be computed up to a max zarnike's order. 
    
    return= a list containing the Zernike basis 
    '''
    step_size = radius*2/shape
    center = (shape-1)/2 # Center of the map
    rrange = radius/step_size # it gives how many steps there are in a radius.
    zernikes=[] # list of Zernike maps
    
    # prep phi element
    def theta(x,y):
        phi = np.arctan2(y, x)
        return phi
    
    # prep radil part. 
    def radial(x,y,n,m):
        # we keep m positive just to compute the radial part.
        if m<0:
            m=-m
            
        # for each n,m couple the radial part is a sum of elements. 
        sum=0
        norm_rad = ((x**2+y**2) / rrange**2 ) **(1/2)   # normalizing the radius from a step point of view. 
        # radial component, we now collect all the radial parts, without apply the rule for (n-m) as odd value:
        for h in range(int((n-m)/2)+1):  
            # using step-normalized radius 
            r=((-1)**h) * (factorial(n-h)) / (factorial(h) * factorial((n+m)/2-h) * factorial((n-m)/2-h)) * ((norm_rad)**(n-2*h))
            # updating the sum                                                                                                
            sum+=r
        # return radial function                                                                                                   
        return sum
                                                                                                             
    # sin e cos parts of Zernike 
    def angular(x,y,n,m): 
        a=theta(x,y)
        if m>=0:
            angular=np.cos(m*a)
        else:
            angular=-np.sin(m*a)  # -sin(-beta) = sin(beta)
        return angular
                                                                                                             
    # for each n,  m should go from -n to n, excluding the values where n-m is odd, where the Zn are zero.
    for n in range(max_zern+1):
        # for each n,m a matrix is build  --------------------                                                                                                 
        for m in range(-n,n+1,2):   # selecting only n-m as even
            # we're centering around zero the Zernike polynomials.                                                                                               
            stepRange = np.arange(shape)-center 
            # prep grid, with sparse=True the matrices x and y will be reduced to a 1d array each. Using them together should make a new matrix?                                                                                                
            x,y=np.meshgrid(stepRange,stepRange,sparse=True)
            # z value in x,y position of the matrix                                                                                                
            zfunc=radial(x,y,n,m)*angular(x,y,n,m)
         #----------------------------------------------------------------
         # Set the values outside the radius to zero                                                                                                    
            for i in range(shape):
                for j in range(shape): 
                    if (i-center)**2+(j-center)**2 >= rrange**2:
                        zfunc[i][j]=0 
                 #------------------------------------------------------------------                                                                                             
            zmap=zfunc/np.abs(zfunc).max() # Such that the amplitude(maximum value in the map data) equals to 1
            zernikes.append(zmap)

    return zernikes # Return Zernike basis up to max_zern

def coeff_An_zernike(real_map,zernikebasis):
    # coeff
    a_n = []
    
    for index,mode in enumerate(zernikebasis):
        a_n_ = (real_map*mode).sum()/(mode**2).sum()
        a_n.append(a_n_)
    # return the A_n coefficients
    return a_n

def zernikeCoeff(data, xl, yl, aperture, order):
    basis = Zernikes(data.shape[0], aperture, order)
    a_n = coeff_An_zernike(data, basis)
    return a_n

def zernikeSum(x, y, coeff, basis, normRadius, outRadius):
    if len(x.shape) < 2 and len(x.shape) < 2:
        x, y = np.meshgrid(x, y)
    else:
        if x.shape != y.shape:
            raise ValueError("x and y need to have the same shape")
    r = np.sqrt(x**2 + y**2)
    r[r > outRadius] = np.nan
    r = r/normRadius
    fSum = np.zeros(x.shape)
    for index in range(len(coeff)):
        fSum += coeff[index]*basis[index]
    return fSum, x, y

def extendOutside(mapIn, xIn, yIn, rfit, rOut, zernikeN = 5):
    if rfit == 0:
        xL, xH, yL, yH = findBoundary(mapIn, xIn, yIn, True)
        rfit = np.min((xH-xL, yH-yL))/2
    coef = zernikeCoeff(mapIn, xIn, yIn, rfit*2, zernikeN)

    if len(xIn.shape) > 1:
        xIn = xIn[0]
    if len(yIn.shape) > 1:
        yIn = yIn.T[0].T
    
    if rOut < 0:
        rOut = -rOut
        xOut = xIn
        yOut = yIn
        N0X = 0
        N0Y = 0
    else:
        dxl = xIn[1] - xIn[0]
        dyl = yIn[1] - yIn[0]
        dN = rOut + xIn[0]
        if dN > 0:
            N0X = int(np.ceil(dN/dyl))
        else:
            N0Y = 0
    N = np.ceil(np.max(rOut-xIn[-1], 0)/dxl) + N0X + len(xIn)
    xOut = xIn[0] + dxl*(np.arange(1, N+1) - N0X - 1)
    dN = rOut + yIn[0]
    if dN > 0:
        N0Y = int(np.ceil(dN/dyl))
    else:
        N0Y = 0
    N = np.ceil(np.max(rOut-yIn[-1], 0)/dyl) + N0Y + len(yIn)
    yOut = yIn[0] + dyl*(np.arange(1, N+1) - N0Y - 1)

    xg, yg = np.meshgrid(xOut, yOut)
    mapOut = np.zeros(xg.shape)
    mapOut[N0Y:N0Y+len(yIn), N0X:N0X+len(xIn)] = mapIn

    dr = xOut[1] - xOut[0]
    rg = np.sqrt(xg**2 + yg**2)
    basis = Zernikes(xg.shape[0], rfit, zernikeN)
    datFit, x, y = zernikeSum(xg, yg, coef, basis, rfit, rOut+dr)
    rng = np.logical_and(rg >= rfit, rg <= abs(rOut)+dr)
    mapOut[rng] = datFit[rng]

    xeps = 0.1
    wgt = np.sqrt(1-1/xeps*(1-rg/rfit))
    rng = np.logical_and(rg > rfit*(1-xeps), rg < rfit)
    mapOut[rng] = datFit[rng]*wgt[rng] + mapOut[rng]*(1-wgt[rng])
    return mapOut, xOut, yOut

    
    

