%* Alfi_Version 6.0.14
Add_Submodules
{
    xyz2clamp Wrap
    clamp2xyz Unwrap
    data_in offset
    data_in speed
    data_in amp
    data_in freq
    data_in phase
    data_in dim
    data_in gain
    data_in t_on
    data_out OutDim
    data_out InDim
    FUNC_X_16x16 FiddleX
}
Settings Wrap
{
%*  GUI_Settings
%*  {
%*      Origin 408x72
%*  }
}
Settings Unwrap
{
%*  GUI_Settings
%*  {
%*      Origin 66x72
%*  }
}
Settings offset
{
%*  GUI_Settings
%*  {
%*      Origin 72x180
%*  }
}
Settings speed
{
%*  GUI_Settings
%*  {
%*      Origin 114x198
%*  }
}
Settings amp
{
%*  GUI_Settings
%*  {
%*      Origin 72x216
%*  }
}
Settings freq
{
%*  GUI_Settings
%*  {
%*      Origin 114x234
%*  }
}
Settings phase
{
%*  GUI_Settings
%*  {
%*      Origin 72x252
%*  }
}
Settings dim
{
    #quote_begin
init = 2
    #quote_end

%*  GUI_Settings
%*  {
%*      Origin 114x270
%*  }
}
Settings gain
{
    #quote_begin
init = 1
    #quote_end

%*  GUI_Settings
%*  {
%*      Origin 72x288
%*  }
}
Settings t_on
{
%*  GUI_Settings
%*  {
%*      Origin 114x306
%*  }
}
Settings OutDim
{
%*  GUI_Settings
%*  {
%*      Origin 420x186
%*      Display = Name
%*  }
}
Settings InDim
{
%*  GUI_Settings
%*  {
%*      Origin 420x222
%*      Display = Name
%*  }
}
Settings FiddleX
{
    #quote_begin
MemberDecl = /* fiddle integrators */
DF df_pos;
DF df_phase;
    #quote_end

    #quote_begin
Equations = #define input( index ) (*in[index])
#define output(index) ( (*out)[index] )

enum { 
xIndex, yIndex, zIndex, theXIndex, theYIndex, theZIndex,
offsetIndex, speedIndex, ampIndex, freqIndex, phaseIndex, dimIndex,
gainIndex, tOnIndex };

size_t dimension = static_cast<size_t>( floor( input(dimIndex) + 0.5 ) );

adlib_real phase = input( phaseIndex )
+ df_phase.filterApply( 2*M_PI* input(freqIndex) );

adlib_real pos = input( offsetIndex ) + df_pos.filterApply( input(speedIndex) ) + input(ampIndex)*sin(phase);

/* outputs */
if ( get_time_now() < input(tOnIndex) )
{
  for( size_t index = 0; index < 6; index++ )
    output(index) = 0;
}
else
{
  for( size_t index = 0; index < 6; index++ )
  {
    output( index ) = input(index) * input(gainIndex);;
  }
  output( dimension ) += input(gainIndex) * pos;
}

enum { OutDimIndex = theZIndex+1, InDimIndex };
output( OutDimIndex ) = output( dimension );
output( InDimIndex ) = input( dimension );
    #quote_end

    #quote_begin
Constructor = adlib_real pole0 = 0;

df_pos.addKZP( 1, 0, NULL, 1, &pole0 );
df_phase.addKZP( 1, 0, NULL, 1, &pole0 );
    #quote_end

%*  GUI_Settings
%*  {
%*      Origin 282x72
%*  }
}
Add_Connections    % Modeler only (ignored by Alfi.)
{
    this in -> Unwrap 0
    Wrap 0 -> this out
    Unwrap Y -> FiddleX in1
    Unwrap thetaX -> FiddleX in3
    Unwrap thetaY -> FiddleX in4
    Unwrap thetaZ -> FiddleX in5
    FiddleX out0 -> Wrap X
    FiddleX out1 -> Wrap Y
    FiddleX out3 -> Wrap thetaX
    FiddleX out4 -> Wrap thetaY
    FiddleX out5 -> Wrap thetaZ
    offset 0 -> FiddleX in6
    speed 0 -> FiddleX in7
    amp 0 -> FiddleX in8
    freq 0 -> FiddleX in9
    phase 0 -> FiddleX in10
    Unwrap X -> FiddleX in0
    Unwrap Z -> FiddleX in2
    FiddleX out2 -> Wrap Z
    FiddleX out6 -> OutDim 0
    FiddleX out7 -> InDim 0
    dim 0 -> FiddleX in11
    gain 0 -> FiddleX in12
    t_on 0 -> FiddleX in13
}
%*Add_Connections_gui
%*{
%*    this in -> Unwrap 0
%*    { }
%*    Wrap 0 -> this out
%*    { }
%*    Unwrap Y -> FiddleX in1
%*    { }
%*    Unwrap thetaX -> FiddleX in3
%*    { }
%*    Unwrap thetaY -> FiddleX in4
%*    { }
%*    Unwrap thetaZ -> FiddleX in5
%*    { }
%*    FiddleX out0 -> Wrap X
%*    { }
%*    FiddleX out1 -> Wrap Y
%*    { }
%*    FiddleX out3 -> Wrap thetaX
%*    { }
%*    FiddleX out4 -> Wrap thetaY
%*    { }
%*    FiddleX out5 -> Wrap thetaZ
%*    { }
%*    offset 0 -> FiddleX in6
%*    {
%*        x=186
%*    }
%*    speed 0 -> FiddleX in7
%*    {
%*        x=192
%*    }
%*    amp 0 -> FiddleX in8
%*    {
%*        x=198
%*    }
%*    freq 0 -> FiddleX in9
%*    {
%*        x=204
%*    }
%*    phase 0 -> FiddleX in10
%*    {
%*        x=210
%*    }
%*    Unwrap X -> FiddleX in0
%*    { }
%*    Unwrap Z -> FiddleX in2
%*    { }
%*    FiddleX out2 -> Wrap Z
%*    { }
%*    FiddleX out6 -> OutDim 0
%*    {
%*        x=402
%*    }
%*    FiddleX out7 -> InDim 0
%*    {
%*        x=384
%*    }
%*    dim 0 -> FiddleX in11
%*    {
%*        x=216
%*    }
%*    gain 0 -> FiddleX in12
%*    {
%*        x=228
%*    }
%*    t_on 0 -> FiddleX in13
%*    {
%*        x=234
%*    }
%*}
%*Port input in
%*{
%*    dataType = clamp
%*    internalOrientation west 0
%*    externalOrientation west 0
%*}
%*Port output out
%*{
%*    dataType = clamp
%*    internalOrientation east 0
%*    externalOrientation east 0
%*}
%*GUI_Settings
%*{
%*    ScreenSize 631x465
%*}
