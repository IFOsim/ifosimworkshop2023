#!/usr/bin/env python


# %% Import Statements
#from __future__ import division
import math
import matplotlib
#matplotlib.use("Qt4Agg")
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.signal as ssig
import os
import sys


# %% Graphical Options
# Get this from LaTeX using \showthe\columnwidth
fig_width_pt = 600
# Convert pt to inch
inches_per_pt = 1.0/72.27
# Aesthetic ratio
#golden_mean = (2.236-1.0)/2.0
golden_mean = 0.6
# Width in inches
fig_width = fig_width_pt*inches_per_pt
# Height in inches
fig_height = fig_width*golden_mean
# Figure size
fig_size = [fig_width,fig_height]
# matplotlib options
matplotlib.rcParams.update({'agg.path.chunksize' : 100000,
                            'savefig.dpi' : 350,
                            'text.usetex' : True,
                            'figure.figsize' : fig_size,
                            'font.family' : "serif",
                            'font.serif' : ["Times"],
                            'font.size' : 14})


# %% Definitions
# Container that allows attribute access, similar to Matlab struct
class Struc(object):
    """simple container that allows attribute access, similar to Matlab struct"""
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

# Class used to read the E2E output files
class read_e2e:
    def __init__(self, name):
        name = os.path.splitext(name)[0]
        try:
            try:
                import pandas as pd
                #dat = pd.read_csv(name + '.dat.gz', compression='gzip', delim_whitespace=True, header=None)
                dat = pd.read_csv(name + '.dat', delim_whitespace=True, header=None)
                self.data = np.array(dat)
            except:
                print("WARNING: Pandas module not available: defaulting back to numpy.genfromtxt ...")
                self.data = np.genfromtxt(name + '.dat')
        except:
            sys.exit('Error: could not read %s.dat' % name)
        try:
            f = open(name + '.dhr')
            self.headers = [line.strip() for line in f]
            f.close()
        except:
            print('Error: could not read %s.dhr' % name)
            sys.exit(0)
        for i, chan in enumerate(self.headers):
            chan = chan.replace('.', '_')
            self.__dict__[chan] = self.data[:,i]

# Read file with matlab style parameters
def eval_in_struct(filename):
    'evaluates a file with simple matlab definitions and returns results in a Bunch'
    lns = {}
    gns = math.__dict__
    with open(filename) as fin:
        for line in fin:
            line, _, _ = line.partition('%')  # strip Matlab comments
            line = line.strip()  # remove indents
            if not line:  # skip emtpy line
                continue
            assert '=' in line
            assert '^' not in line  # TODO: convert ^ to **
            par, _, expr = line.partition('=')
            par = par.strip()
            expr, _, _ = expr.partition(';') # remove trailing ;
            # print '{!r} -> {!r}'.format(par, expr)
            # exec(line, gns, lns)
            lns[par] = eval(expr, gns, lns)
    return Struc(**lns)

# Template to plot multiple data sets
def multiplot(plots, titles = None, ylabels = None, scales = None, xlabel = ''):
    nplots = len(plots)
    if not titles:
        titles = [''] * nplots
    if not ylabels:
        ylabels = [''] * nplots
    if not scales:
        scales = ['linear'] * nplots
    fig, axes = plt.subplots(nplots, 1, sharex=True, figsize=(13.0, 26.0))
    if nplots == 1: axes = [axes] #hack so that you can always iterate over axes
    assert all(len(x) == nplots for x in (axes, titles, ylabels))
    for ax, plot, title, ylabel, scale in zip(axes, plots, titles, ylabels, scales):
        ax.plot(*plot)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.set_yscale(scale)
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.grid()
    
    # Only for last plot
    ax = axes[-1]
    plt.setp(ax.get_xticklabels(), visible=True)
    ax.set_xlabel(xlabel)
    ax.grid()
    return axes

# Template to plot multiple data sets on two columns
def multi2plot(plots, titles = None, ylabels = None, scales = None, xlabel = ''):
    nplots = len(plots)
    if not titles:
        titles = [''] * nplots
    if not ylabels:
        ylabels = [''] * nplots
    if not scales:
        scales = ['linear'] * nplots

    fig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(nplots//2+nplots%2, 2, figure=fig, right=0.8)
    axes = [''] * nplots
    splt_aux = 0
    for splt in np.arange(0,nplots//2):
        if splt == 0:
            axes[splt+splt_aux] = fig.add_subplot(gs[splt, 0])
        else:
            axes[splt+splt_aux] = fig.add_subplot(gs[splt, 0], sharex=axes[0])
        axes[splt+splt_aux+1] = fig.add_subplot(gs[splt, 1], sharex=axes[0])
        splt_aux += 1
            
    # Last (odd) plot or single plot use entire width
    if nplots%2:
        if nplots != 1:
            axes[-1] = fig.add_subplot(gs[nplots//2, :], sharex=axes[0])
        else:
            axes[-1] = fig.add_subplot(gs[nplots//2, :])

    if nplots == 1: axes = [axes] #hack so that you can always iterate over axes
    assert all(len(x) == nplots for x in (axes, titles, ylabels))
    for ax, plot, title, ylabel, scale in zip(axes, plots, titles, ylabels, scales):
        ax.plot(*plot)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.set_yscale(scale)
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.grid()
    
    # Only for last plot
    ax = axes[-1]
    plt.setp(ax.get_xticklabels(), visible=True)
    ax.set_xlabel(xlabel)
    ax.grid()
    return axes

# Autoscale matplotlib plot
class AutoScaleY():
    def  __init__(self, line, margin=0.05):
        self.margin = margin
        self.line = line
        self.ax = line.axes
        self.ax.callbacks.connect('xlim_changed', self.rescale_y)

    def rescale_y(self,evt=None):
        xmin, xmax = self.ax.get_xlim()
        x, y = self.line.get_data()
        cond = (x >= xmin) & (x <= xmax)
        yrest = y[cond]
        margin = (yrest.max()-yrest.min())*self.margin
        self.ybounds = [yrest.min()-margin, yrest.max()+margin]
        self.timer = self.ax.figure.canvas.new_timer(interval=10)
        self.timer.single_shot = True
        self.timer.add_callback(self.change_y)
        self.timer.start()

    def change_y(self):
        self.ax.set_ylim(self.ybounds)
        self.ax.figure.canvas.draw()

# Normalize a signal, if possible
def normalize_signal(signal):
    if np.max(signal) > 0:
        return signal/np.max(signal)
    else:
        print("WARNING: selected signal is never positive!")
        return signal


# %% Data Path
data_set = 'ET_LF_Arm'

data_dir = '/home/blind/Downloads/'+data_set


# %% E2E Data
d = read_e2e(data_dir+'/Lock')
tt = d.time[[0,-1]]
xx = [0,0]


# %% PLOT
#plt.style.use('dark_background')
#plt.style.use('default')
axes = multi2plot([
    #[d.time, d.main_B2_6MHz_DC, 'b', d.time[ind], d.main_B2_6MHz_DC[ind], '.r'],
    [d.time, d.ET_LF_Arm_MechITM_z, 'b'],
    [d.time, d.ET_LF_Arm_Sensors_REFL_DC, 'b'],
    [d.time, d.ET_LF_Arm_Sensors_TRANS_DC, 'b'],
    [d.time, d.ET_LF_Arm_Sensors_REFL_I, 'b'],
    [d.time, d.ET_LF_Arm_Control_CTRL, 'b'],
    [d.time, d.ET_LF_Arm_MechITM_f_press, 'b'],
    [d.time, d.ET_LF_Arm_MechITM_f_tot, 'b'],
    ],
    titles = ['ITM z', 'REFL_DC','TRANS_DC','REFL_I','CTRL', 'ITM f_press', 'ITM f_tot'],
    #titles = ['ITM z', 'REFL_DC','TRANS_DC','REFL_I','CTRL'],
    ylabels = ['[$m$]', '[W]', '[mW]', '[a.u.]', '[a.u.]', '[N]', '[N]'],
    #ylabels = ['[$\mu m$]', '[mW]', '[mW]', '[a.u.]', '[a.u.]'],
    scales = ['linear', 'linear', 'linear', 'linear', 'linear', 'linear', 'linear'],
    #scales = ['linear', 'linear', 'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
plt.xlim([0, d.time[-1]])
plt.xticks([0, d.time[-1]/4, d.time[-1]/2, d.time[-1]],[str(0), str(d.time[-1]/4), str(d.time[-1]/2), str(d.time[-1])]) 
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
#plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- DOF Sel. Rules",fontsize=22)
plt.gcf().suptitle("Preliminar Study of ET LF Arm Cavity",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
plt.savefig(data_dir+'/'+data_set+'.png', dpi=300, bbox_inches='tight')
