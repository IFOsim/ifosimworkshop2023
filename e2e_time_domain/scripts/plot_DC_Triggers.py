#!/usr/bin/env python


# %% Import Statements
#from __future__ import division
import math
import matplotlib
#matplotlib.use("Qt4Agg")
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.signal as ssig
import os
import sys


# %% Graphical Options
# Get this from LaTeX using \showthe\columnwidth
fig_width_pt = 600
# Convert pt to inch
inches_per_pt = 1.0/72.27
# Aesthetic ratio
#golden_mean = (2.236-1.0)/2.0
golden_mean = 0.6
# Width in inches
fig_width = fig_width_pt*inches_per_pt
# Height in inches
fig_height = fig_width*golden_mean
# Figure size
fig_size = [fig_width,fig_height]
# matplotlib options
matplotlib.rcParams.update({'agg.path.chunksize' : 100000,
                            'savefig.dpi' : 350,
                            'text.usetex' : True,
                            'figure.figsize' : fig_size,
                            'font.family' : "serif",
                            'font.serif' : ["Times"],
                            'font.size' : 14})


# %% Definitions
# Container that allows attribute access, similar to Matlab struct
class Struc(object):
    """simple container that allows attribute access, similar to Matlab struct"""
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

# Class used to read the E2E output files
class read_e2e:
    def __init__(self, name):
        name = os.path.splitext(name)[0]
        try:
            try:
                import pandas as pd
                dat = pd.read_csv(name + '.dat.gz', compression='gzip', delim_whitespace=True, header=None)
                self.data = np.array(dat)
            except:
                print("WARNING: Pandas module not available: defaulting back to numpy.genfromtxt ...")
                self.data = np.genfromtxt(name + '.dat')
        except:
            sys.exit('Error: could not read %s.dat' % name)
        try:
            f = open(name + '.dhr')
            self.headers = [line.strip() for line in f]
            f.close()
        except:
            print('Error: could not read %s.dhr' % name)
            sys.exit(0)
        for i, chan in enumerate(self.headers):
            chan = chan.replace('.', '_')
            self.__dict__[chan] = self.data[:,i]

# Read file with matlab style parameters
def eval_in_struct(filename):
    'evaluates a file with simple matlab definitions and returns results in a Bunch'
    lns = {}
    gns = math.__dict__
    with open(filename) as fin:
        for line in fin:
            line, _, _ = line.partition('%')  # strip Matlab comments
            line = line.strip()  # remove indents
            if not line:  # skip emtpy line
                continue
            assert '=' in line
            assert '^' not in line  # TODO: convert ^ to **
            par, _, expr = line.partition('=')
            par = par.strip()
            expr, _, _ = expr.partition(';') # remove trailing ;
            # print '{!r} -> {!r}'.format(par, expr)
            # exec(line, gns, lns)
            lns[par] = eval(expr, gns, lns)
    return Struc(**lns)

# Template to plot multiple data sets
def multiplot(plots, titles = None, ylabels = None, scales = None, xlabel = ''):
    nplots = len(plots)
    if not titles:
        titles = [''] * nplots
    if not ylabels:
        ylabels = [''] * nplots
    if not scales:
        scales = ['linear'] * nplots
    fig, axes = plt.subplots(nplots, 1, sharex=True, figsize=(13.0, 26.0))
    if nplots == 1: axes = [axes] #hack so that you can always iterate over axes
    assert all(len(x) == nplots for x in (axes, titles, ylabels))
    for ax, plot, title, ylabel, scale in zip(axes, plots, titles, ylabels, scales):
        ax.plot(*plot)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.set_yscale(scale)
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.grid()
    
    # Only for last plot
    ax = axes[-1]
    plt.setp(ax.get_xticklabels(), visible=True)
    ax.set_xlabel(xlabel)
    ax.grid()
    return axes

# Template to plot multiple data sets on two columns
def multi2plot(plots, titles = None, ylabels = None, scales = None, xlabel = ''):
    nplots = len(plots)
    if not titles:
        titles = [''] * nplots
    if not ylabels:
        ylabels = [''] * nplots
    if not scales:
        scales = ['linear'] * nplots

    fig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(nplots//2+nplots%2, 2, figure=fig, right=0.8)
    axes = [''] * nplots
    splt_aux = 0
    for splt in np.arange(0,nplots//2):
        if splt == 0:
            axes[splt+splt_aux] = fig.add_subplot(gs[splt, 0])
        else:
            axes[splt+splt_aux] = fig.add_subplot(gs[splt, 0], sharex=axes[0])
        axes[splt+splt_aux+1] = fig.add_subplot(gs[splt, 1], sharex=axes[0])
        splt_aux += 1
            
    # Last (odd) plot or single plot use entire width
    if nplots%2:
        if nplots != 1:
            axes[-1] = fig.add_subplot(gs[nplots//2, :], sharex=axes[0])
        else:
            axes[-1] = fig.add_subplot(gs[nplots//2, :])

    if nplots == 1: axes = [axes] #hack so that you can always iterate over axes
    assert all(len(x) == nplots for x in (axes, titles, ylabels))
    for ax, plot, title, ylabel, scale in zip(axes, plots, titles, ylabels, scales):
        ax.plot(*plot)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        ax.set_yscale(scale)
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.grid()
    
    # Only for last plot
    ax = axes[-1]
    plt.setp(ax.get_xticklabels(), visible=True)
    ax.set_xlabel(xlabel)
    ax.grid()
    return axes

# Autoscale matplotlib plot
class AutoScaleY():
    def  __init__(self, line, margin=0.05):
        self.margin = margin
        self.line = line
        self.ax = line.axes
        self.ax.callbacks.connect('xlim_changed', self.rescale_y)

    def rescale_y(self,evt=None):
        xmin, xmax = self.ax.get_xlim()
        x, y = self.line.get_data()
        cond = (x >= xmin) & (x <= xmax)
        yrest = y[cond]
        margin = (yrest.max()-yrest.min())*self.margin
        self.ybounds = [yrest.min()-margin, yrest.max()+margin]
        self.timer = self.ax.figure.canvas.new_timer(interval=10)
        self.timer.single_shot = True
        self.timer.add_callback(self.change_y)
        self.timer.start()

    def change_y(self):
        self.ax.set_ylim(self.ybounds)
        self.ax.figure.canvas.draw()

# Function to find all the points equal to the ones selected on the DOF basis
def find_inv_equal(signal, ind_all, ind_sel):
    sel_arr_tot = np.zeros(len(ind_all), dtype=bool)
    for val in signal[ind_sel]:
        sel_arr_tot[np.where(signal[ind_all]==val)[0]] = True
    return sel_arr_tot

# Function to find all the points in the same range to the ones selected on the DOF basis
def find_inv_range(signal, ind_all, ind_sel):
    sel_arr_min = np.zeros(len(ind_all), dtype=bool)
    sel_arr_max = np.zeros(len(ind_all), dtype=bool)
    sel_arr_tot = np.zeros(len(ind_all), dtype=bool)
    sel_arr_min[np.where(signal[ind_all]>=np.min(signal[ind_sel]))[0]] = True
    sel_arr_max[np.where(signal[ind_all]<=np.max(signal[ind_sel]))[0]] = True
    sel_arr_tot = sel_arr_min & sel_arr_max
    return sel_arr_tot

# Function to find all the points where NORMALIZED signal is higher or lower than a threshold
def find_thr_norm(signal, ind_all, thr_type, thr_val):
    sel_arr_tot = np.zeros(len(ind_all), dtype=bool)
    if thr_type == 'leq':
        sel_arr_tot[np.where(signal[ind_all]<=thr_val)[0]] = True
    elif thr_type == 'geq':
        sel_arr_tot[np.where(signal[ind_all]>=thr_val)[0]] = True
    else:
        sel_arr_tot[ind_all] = True
        print("ERROR: criterion must be either 'leq' or 'geq'!")  
    return sel_arr_tot

# Normalize a signal, if possible
def normalize_signal(signal):
    if np.max(signal) > 0:
        return signal/np.max(signal)
    else:
        print("WARNING: selected signal is never positive!")
        return signal


# %% Data Path
data_set = '46_scan_SRCL_sine_MP_decim10_27W_SB_3rd'
#data_set = '45_scan_SRCL_wp_0_sine_MP_decim10_27W'
#data_set = '42_scan_SRCL_sine_MP_decim10_27W'
#data_set = '41_scan_SRCL_sine_MP'

data_dir = '/home/blind/Work/Virgo/ISC/200722_DRMI_Triggers/data/'+data_set


# %% E2E Data
d = read_e2e(data_dir+'/output')
tt = d.time[[0,-1]]
xx = [0,0]


# %% Computations
# Compute 6MHz 2f sideband power
main_B2_12MHz_MAG = np.sqrt(np.power(d.main_B2_12MHz_I,2)+np.power(d.main_B2_12MHz_Q,2))
main_B4_12MHz_MAG = np.sqrt(np.power(d.main_B4_12MHz_I,2)+np.power(d.main_B4_12MHz_Q,2))
main_B1p_12MHz_MAG = np.sqrt(np.power(d.main_B1p_12MHz_I,2)+np.power(d.main_B1p_12MHz_Q,2))
main_B5_12MHz_MAG = np.sqrt(np.power(d.main_B5_12MHz_I,2)+np.power(d.main_B5_12MHz_Q,2))
main_B5p_12MHz_MAG = np.sqrt(np.power(d.main_B5p_12MHz_I,2)+np.power(d.main_B5p_12MHz_Q,2))

# Compute 56MHz 2f sideband power
main_B2_112MHz_MAG = np.sqrt(np.power(d.main_B2_112MHz_I,2)+np.power(d.main_B2_112MHz_Q,2))
main_B4_112MHz_MAG = np.sqrt(np.power(d.main_B4_112MHz_I,2)+np.power(d.main_B4_112MHz_Q,2))
main_B1p_112MHz_MAG = np.sqrt(np.power(d.main_B1p_112MHz_I,2)+np.power(d.main_B1p_112MHz_Q,2))
main_B5_112MHz_MAG = np.sqrt(np.power(d.main_B5_112MHz_I,2)+np.power(d.main_B5_112MHz_Q,2))
main_B5p_112MHz_MAG = np.sqrt(np.power(d.main_B5p_112MHz_I,2)+np.power(d.main_B5p_112MHz_Q,2))

# Compute 8MHz 2f sideband power
main_B2_16MHz_MAG = np.sqrt(np.power(d.main_B2_16MHz_I,2)+np.power(d.main_B2_16MHz_Q,2))
main_B4_16MHz_MAG = np.sqrt(np.power(d.main_B4_16MHz_I,2)+np.power(d.main_B4_16MHz_Q,2))
main_B1p_16MHz_MAG = np.sqrt(np.power(d.main_B1p_16MHz_I,2)+np.power(d.main_B1p_16MHz_Q,2))
main_B5_16MHz_MAG = np.sqrt(np.power(d.main_B5_16MHz_I,2)+np.power(d.main_B5_16MHz_Q,2))
main_B5p_16MHz_MAG = np.sqrt(np.power(d.main_B5p_16MHz_I,2)+np.power(d.main_B5p_16MHz_Q,2))

# Normalize DOFs w.r.t. the wavelength
main_SUS_MICH_z_out =  d.main_SUS_MICH_z_out/1.064e-6
main_SUS_PRCL_z_out =  d.main_SUS_PRCL_z_out/1.064e-6
main_SUS_SRCL_z_out =  d.main_SUS_SRCL_z_out/1.064e-6
# Cut the initial build-up phase
cut = np.where(np.array(d.time)>=1.0)[0]


# %% DOF Selection Rules
# DOF normalized threshold
dof_thr = 0.005
# Single DOF rules
doft_M = np.abs(main_SUS_MICH_z_out[cut]) <= dof_thr
doft_P = np.abs(main_SUS_PRCL_z_out[cut]) <= dof_thr
doft_S = np.abs(main_SUS_SRCL_z_out[cut]+0.25) <= dof_thr
#doft_S = np.abs(main_SUS_SRCL_z_out[cut]+0.0) <= dof_thr

# Build final selection rule
#ind = cut[doft_P]
#ind = cut[doft_M & doft_P]
ind = cut[doft_M & doft_P & doft_S]

# Print SRCL working point
SRCL_X, SRCL_Y = d.time[ind[len(ind)//2]], main_SUS_SRCL_z_out[ind[len(ind)//2]]
#SRCL_X, SRCL_Y = d.time[ind], main_SUS_SRCL_z_out[ind]
#SRCL_XD, SRCL_YD = ax.transData.transform((SRCL_X, SRCL_Y))
print("SRCL_SET = ",SRCL_Y)


# %% VERY TEMP & VERY WIP for the Paper
#plt.style.use('dark_background')
#plt.style.use('default')
axes = multi2plot([
    #[d.time, d.main_B2_6MHz_DC, 'b', d.time[ind], d.main_B2_6MHz_DC[ind], '.r'],
    [d.time, d.main_B4_6MHz_DC, 'b', d.time[ind], d.main_B4_6MHz_DC[ind], '.r'],
    #[d.time, d.main_B5_6MHz_DC, 'b', d.time[ind], d.main_B5_6MHz_DC[ind], '.r'],
    [d.time, d.main_B1p_6MHz_DC, 'b', d.time[ind], d.main_B1p_6MHz_DC[ind], '.r'],
    #[d.time, d.main_B2_6MHz_I, 'b', d.time[ind], d.main_B2_6MHz_I[ind], '.r'],
    #[d.time, d.main_B2_6MHz_Q, 'b', d.time[ind], d.main_B2_6MHz_Q[ind], '.r'],
    #[d.time, d.main_B2_56MHz_I, 'b', d.time[ind], d.main_B2_56MHz_I[ind], '.r'],
    #[d.time, d.main_B2_56MHz_Q, 'b', d.time[ind], d.main_B2_56MHz_Q[ind], '.r'],
    #[d.time, d.main_B2_8MHz_I, 'b', d.time[ind], d.main_B2_8MHz_I[ind], '.r'],
    #[d.time, d.main_B2_8MHz_Q, 'b', d.time[ind], d.main_B2_8MHz_Q[ind], '.r'],
    [d.time, main_B4_12MHz_MAG, 'b', d.time[ind], main_B4_12MHz_MAG[ind], '.r'],
    [d.time, main_B4_112MHz_MAG, 'b', d.time[ind], main_B4_112MHz_MAG[ind], '.r'],
    [d.time, main_SUS_MICH_z_out, 'b', d.time[ind], main_SUS_MICH_z_out[ind], '.r'],
    [d.time, main_SUS_PRCL_z_out, 'b', d.time[ind], main_SUS_PRCL_z_out[ind], '.r'],
    [d.time, main_SUS_SRCL_z_out, 'b', d.time[ind], main_SUS_SRCL_z_out[ind], '.r'],
    ],
    #titles = ['B2 DC', 'B4 DC', 'B5 DC', 'B1p DC',
    #          'B2 6MHz I', 'B2 6MHz Q', 'B2 56MHz I', 'B2 56MHz Q', 'B2 8MHz I', 'B2 8MHz Q',
    #          'B4 12MHz MAG', 'B4 112MHz MAG',
    #          'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    titles = ['B4 DC', 'B1p DC',
              'B4 12MHz MAG', 'B4 112MHz MAG',
              'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    ylabels = ['[a.u.]', '[mA]',
               '[a.u.]', '[a.u.]',
               '$\lambda$', '$\lambda$', '$\lambda$'],
    scales = ['log', 'linear',
              'linear', 'linear',
              'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
plt.xlim([0, d.time[-1]])
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
#plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- DOF Sel. Rules",fontsize=22)
plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- DOF Sel. Rules",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
plt.savefig('/home/blind/Downloads/test_CAR_lock.png', dpi=300, bbox_inches='tight')



# %% Plot Signals with DOF Selection Rules
#plt.style.use('dark_background')
#plt.style.use('default')
axes = multi2plot([
    [d.time, d.main_B2_6MHz_DC, 'b', d.time[ind], d.main_B2_6MHz_DC[ind], '.r'],
    [d.time, d.main_B4_6MHz_DC, 'b', d.time[ind], d.main_B4_6MHz_DC[ind], '.r'],
    [d.time, d.main_B5_6MHz_DC, 'b', d.time[ind], d.main_B5_6MHz_DC[ind], '.r'],
    [d.time, d.main_B1p_6MHz_DC, 'b', d.time[ind], d.main_B1p_6MHz_DC[ind], '.r'],
    [d.time, main_B2_12MHz_MAG, 'b', d.time[ind], main_B2_12MHz_MAG[ind], '.r'],
    [d.time, main_B2_112MHz_MAG, 'b', d.time[ind], main_B2_112MHz_MAG[ind], '.r'],
    [d.time, main_B4_12MHz_MAG, 'b', d.time[ind], main_B4_12MHz_MAG[ind], '.r'],
    [d.time, main_B4_112MHz_MAG, 'b', d.time[ind], main_B4_112MHz_MAG[ind], '.r'],
    [d.time, main_B5_12MHz_MAG, 'b', d.time[ind], main_B5_12MHz_MAG[ind], '.r'],
    [d.time, main_B5_112MHz_MAG, 'b', d.time[ind], main_B5_112MHz_MAG[ind], '.r'],
    [d.time, main_B2_16MHz_MAG, 'b', d.time[ind], main_B2_16MHz_MAG[ind], '.r'],
    [d.time, main_B4_16MHz_MAG, 'b', d.time[ind], main_B4_16MHz_MAG[ind], '.r'],
    [d.time, main_SUS_MICH_z_out, 'b', d.time[ind], main_SUS_MICH_z_out[ind], '.r'],
    [d.time, main_SUS_PRCL_z_out, 'b', d.time[ind], main_SUS_PRCL_z_out[ind], '.r'],
    [d.time, main_SUS_SRCL_z_out, 'b', d.time[ind], main_SUS_SRCL_z_out[ind], '.r'],
    ],
    titles = ['B2 DC', 'B4 DC', 'B5 DC', 'B1p DC', 'B2 12MHz MAG', 'B2 112MHz MAG',
              'B4 12MHz MAG', 'B4 112MHz MAG', 'B5 12MHz MAG', 'B5 112MHz MAG', 
              'B2 16MHz MAG', 'B4 16MHz MAG',
              'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    ylabels = ['[W]', '[W]', '[W]', '[W]', '[a.u.]', '[a.u.]',
               '[a.u.]', '[a.u.]', '[a.u.]', '[a.u.]', 
               '[a.u.]', '[a.u.]',
               '$\lambda$', '$\lambda$', '$\lambda$'],
    scales = ['log', 'log', 'log', 'linear', 'linear', 'linear',
              'linear', 'linear', 'linear', 'linear',
              'linear', 'linear',
              'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
plt.xlim([0, d.time[-1]])
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- DOF Sel. Rules",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
#plt.savefig(data_dir+'/DC_Trig_01_DOF_Sel.png', dpi=300, bbox_inches='tight')


# %% Selection Rules for False Positives -- RANGE Criterion
# Inverse rule for B2_DC
dct_B2_DC = find_inv_range(d.main_B2_6MHz_DC,cut,ind)

# Inverse rule for B2_12MHz_mag
dct_B2_12 = find_inv_range(main_B2_12MHz_MAG,cut,ind)

# Inverse rule for B2_112MHz_mag
dct_B2_112 = find_inv_range(main_B2_112MHz_MAG,cut,ind)

# Inverse rule for B2_16MHz_mag
dct_B2_16 = find_inv_range(main_B2_16MHz_MAG,cut,ind)

# Inverse rule for B4_DC
dct_B4_DC = find_inv_range(d.main_B4_6MHz_DC,cut,ind)

# Inverse rule for B4_12MHz_mag
dct_B4_12 = find_inv_range(main_B4_12MHz_MAG,cut,ind)

# Inverse rule for B4_112MHz_mag
dct_B4_112 = find_inv_range(main_B4_112MHz_MAG,cut,ind)

# Inverse rule for B4_16MHz_mag
dct_B4_16 = find_inv_range(main_B4_16MHz_MAG,cut,ind)

# Inverse rule for B1p_DC
dct_B1p_DC = find_inv_range(d.main_B1p_6MHz_DC,cut,ind)

# Inverse rule for B5_DC
dct_B5_DC = find_inv_range(d.main_B5_6MHz_DC,cut,ind)

# Inverse rule for B5_12MHz_mag
dct_B5_12 = find_inv_range(main_B5_12MHz_MAG,cut,ind)

# Inverse rule for B5_112MHz_mag
dct_B5_112 = find_inv_range(main_B5_112MHz_MAG,cut,ind)

# Inverse rule for B5_16MHz_mag
dct_B5_16 = find_inv_range(main_B5_16MHz_MAG,cut,ind)


# %% Build final selection rule
#inv = cut[dct_B4_12]
#inv = cut[dct_B4_12 & dct_B2_112]

inv = cut[dct_B4_12]
inv = cut[dct_B4_12 & dct_B1p_DC]
#inv = cut[dct_B4_12 & dct_B1p_DC & dct_B5_DC]
inv = cut[dct_B4_12 & dct_B1p_DC & dct_B4_112 & dct_B4_DC]


# %% Plot Signals with Inverse Signals Selection Rules
#plt.style.use('dark_background')
#plt.style.use('default')
axes = multi2plot([
    #[d.time, d.main_B2_6MHz_DC, 'b', d.time[inv], d.main_B2_6MHz_DC[inv], '.r'],
    [d.time, d.main_B4_6MHz_DC, 'b', d.time[inv], d.main_B4_6MHz_DC[inv], '.g'],
    #[d.time, d.main_B5_6MHz_DC, 'b', d.time[inv], d.main_B5_6MHz_DC[inv], '.r'],
    [d.time, d.main_B1p_6MHz_DC, 'b', d.time[inv], d.main_B1p_6MHz_DC[inv], '.g'],
    #[d.time, main_B2_12MHz_MAG, 'b', d.time[inv], main_B2_12MHz_MAG[inv], '.r'],
    #[d.time, main_B2_112MHz_MAG, 'b', d.time[inv], main_B2_112MHz_MAG[inv], '.r'],
    [d.time, main_B4_12MHz_MAG, 'b', d.time[inv], main_B4_12MHz_MAG[inv], '.g'],
    [d.time, main_B4_112MHz_MAG, 'b', d.time[inv], main_B4_112MHz_MAG[inv], '.g'],
    #[d.time, main_B5_12MHz_MAG, 'b', d.time[inv], main_B5_12MHz_MAG[inv], '.r'],
    #[d.time, main_B5_112MHz_MAG, 'b', d.time[inv], main_B5_112MHz_MAG[inv], '.r'],
    #[d.time, main_B2_16MHz_MAG, 'b', d.time[inv], main_B2_16MHz_MAG[inv], '.r'],
    #[d.time, main_B4_16MHz_MAG, 'b', d.time[inv], main_B4_16MHz_MAG[inv], '.r'],
    [d.time, main_SUS_MICH_z_out, 'b', d.time[inv], main_SUS_MICH_z_out[inv], '.r'],
    [d.time, main_SUS_PRCL_z_out, 'b', d.time[inv], main_SUS_PRCL_z_out[inv], '.r'],
    [d.time, main_SUS_SRCL_z_out, 'b', d.time[inv], main_SUS_SRCL_z_out[inv], '.r'],
    ],
    #titles = ['B2 DC', 'B4 DC', 'B5 DC', 'B1p DC',
    #          'B2 6MHz I', 'B2 6MHz Q', 'B2 56MHz I', 'B2 56MHz Q', 'B2 8MHz I', 'B2 8MHz Q',
    #          'B4 12MHz MAG', 'B4 112MHz MAG',
    #          'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    titles = ['B4 DC', 'B1p DC',
              'B4 12MHz MAG', 'B4 112MHz MAG',
              'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    ylabels = ['[a.u.]', '[mA]',
               '[a.u.]', '[a.u.]',
               '$\lambda$', '$\lambda$', '$\lambda$'],
    scales = ['log', 'linear',
              'linear', 'linear',
              'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
plt.xlim([0, d.time[-1]])
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
leg_dof = plt.Line2D((0,0),(0,0), color='r', marker='o', linestyle='')
leg_sig = plt.Line2D((0,0),(0,0), color='g', marker='o', linestyle='')
#axes[-1].legend(["DOF Selection","Inverse Trigger Sel."],fontsize=16)
axes[-1].legend([leg_dof,leg_sig],["DOF Selection","Inverse Trigger Sel."],fontsize=16)
#plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- Inverse Signals Selection Rules ("+
#                   str(len(inv))+"/"+str(len(cut))+" pts.)",fontsize=22)
plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- Inverse Signals Selection Rules",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
#plt.savefig(data_dir+'/DC_Trig_02_Inv_DC_Sel.png', dpi=300, bbox_inches='tight')
plt.savefig('/home/blind/Downloads/test_CAR_lock1.png', dpi=300, bbox_inches='tight')



# %% Normalize Signals
# B2 signals
B2_6MHz_DC_NORM = normalize_signal(d.main_B2_6MHz_DC)
B2_12MHz_MAG_NORM = normalize_signal(main_B2_12MHz_MAG)
B2_112MHz_MAG_NORM = normalize_signal(main_B2_112MHz_MAG)
B2_16MHz_MAG_NORM = normalize_signal(main_B2_16MHz_MAG)

# B4 signals
B4_6MHz_DC_NORM = normalize_signal(d.main_B4_6MHz_DC)
B4_12MHz_MAG_NORM = normalize_signal(main_B4_12MHz_MAG)
B4_112MHz_MAG_NORM = normalize_signal(main_B4_112MHz_MAG)
B4_16MHz_MAG_NORM = normalize_signal(main_B4_16MHz_MAG)

# B1p signals
B1p_6MHz_DC_NORM = normalize_signal(d.main_B1p_6MHz_DC)
B1p_12MHz_MAG_NORM = normalize_signal(main_B1p_12MHz_MAG)
B1p_112MHz_MAG_NORM = normalize_signal(main_B1p_112MHz_MAG)
B1p_16MHz_MAG_NORM = normalize_signal(main_B1p_16MHz_MAG)

# B5 signals
B5_6MHz_DC_NORM = normalize_signal(d.main_B5_6MHz_DC)
B5_12MHz_MAG_NORM = normalize_signal(main_B5_12MHz_MAG)
B5_112MHz_MAG_NORM = normalize_signal(main_B5_112MHz_MAG)
B5_16MHz_MAG_NORM = normalize_signal(main_B5_16MHz_MAG)

# B5p signals
B5p_6MHz_DC_NORM = normalize_signal(d.main_B5p_6MHz_DC)
B5p_12MHz_MAG_NORM = normalize_signal(main_B5p_12MHz_MAG)
B5p_112MHz_MAG_NORM = normalize_signal(main_B5p_112MHz_MAG)
B5p_16MHz_MAG_NORM = normalize_signal(main_B5p_16MHz_MAG)


# %% Print Final Selection Rules
#plt.style.use('dark_background')
#plt.style.use('default')
axes = multi2plot([
    [d.time, B2_6MHz_DC_NORM, 'b', d.time[inv], B2_6MHz_DC_NORM[inv], '.r'],
    [d.time, B4_6MHz_DC_NORM, 'b', d.time[inv], B4_6MHz_DC_NORM[inv], '.r'],
    [d.time, B5_6MHz_DC_NORM, 'b', d.time[inv], B5_6MHz_DC_NORM[inv], '.r'],
    [d.time, B1p_6MHz_DC_NORM, 'b', d.time[inv], B1p_6MHz_DC_NORM[inv], '.g'],
    [d.time, B2_12MHz_MAG_NORM, 'b', d.time[inv], B2_12MHz_MAG_NORM[inv], '.r'],
    [d.time, B2_112MHz_MAG_NORM, 'b', d.time[inv], B2_112MHz_MAG_NORM[inv], '.g'],
    [d.time, B4_12MHz_MAG_NORM, 'b', d.time[inv], B4_12MHz_MAG_NORM[inv], '.g'],
    [d.time, B4_112MHz_MAG_NORM, 'b', d.time[inv], B4_112MHz_MAG_NORM[inv], '.r'],
    [d.time, B5_12MHz_MAG_NORM, 'b', d.time[inv], B5_12MHz_MAG_NORM[inv], '.r'],
    [d.time, B5_112MHz_MAG_NORM, 'b', d.time[inv], B5_112MHz_MAG_NORM[inv], '.r'],
    [d.time, B2_16MHz_MAG_NORM, 'b', d.time[inv], B2_16MHz_MAG_NORM[inv], '.r'],
    [d.time, B4_16MHz_MAG_NORM, 'b', d.time[inv], B4_16MHz_MAG_NORM[inv], '.r'],
    [d.time, main_SUS_MICH_z_out, 'b', d.time[inv], main_SUS_MICH_z_out[inv], '.r'],
    [d.time, main_SUS_PRCL_z_out, 'b', d.time[inv], main_SUS_PRCL_z_out[inv], '.r'],
    [d.time, main_SUS_SRCL_z_out, 'b', d.time[inv], main_SUS_SRCL_z_out[inv], '.r'],
    ],
    titles = ['B2 DC', 'B4 DC', 'B5 DC', 'B1p DC', 'B2 12MHz MAG', 'B2 112MHz MAG',
              'B4 12MHz MAG', 'B4 112MHz MAG', 'B5 12MHz MAG', 'B5 112MHz MAG', 
              'B2 16MHz MAG', 'B4 16MHz MAG',
              'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    ylabels = ['[--]', '[--]', '[--]', '[--]', '[--]', '[--]',
               '[--]', '[--]', '[--]', '[--]', 
               '[--]', '[--]',
               '$\lambda$', '$\lambda$', '$\lambda$'],
    scales = ['log', 'log', 'log', 'linear', 'linear', 'linear',
              'linear', 'linear', 'linear', 'linear',
              'linear', 'linear',
              'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
plt.xlim([0, d.time[-1]])
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
leg_dof = plt.Line2D((0,0),(0,0), color='r', marker='o', linestyle='')
leg_sig = plt.Line2D((0,0),(0,0), color='g', marker='o', linestyle='')
#axes[-1].legend(["DOF Selection","Inverse Trigger Sel."],fontsize=16)
axes[-1].legend([leg_dof,leg_sig],["DOF Selection","Inverse Trigger Sel."],fontsize=16)
plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- Normalized Signals -- Final Selection Rules",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
#plt.savefig(data_dir+'/DC_Trig_03_Norm_DC_Final.png', dpi=300, bbox_inches='tight')


# %% Check a signal's derivatives
SIG_NORM = B4_12MHz_MAG_NORM
SIG_NAME = "B4\_12\_MAG"

SIG_NORM_D1 = ssig.savgol_filter(SIG_NORM, window_length=101, polyorder=2, deriv=1)
SIG_NORM_D2 = ssig.savgol_filter(SIG_NORM, window_length=101, polyorder=2, deriv=2)

#plt.style.use('dark_background')
#plt.style.use('default')
axes = multiplot([
    [d.time, SIG_NORM, 'b', d.time[inv], SIG_NORM[inv], '.r'],
    [d.time, SIG_NORM_D1, 'b', d.time[inv], SIG_NORM_D1[inv], '.r'],
    [d.time, SIG_NORM_D2, 'b', d.time[inv], SIG_NORM_D2[inv], '.r'],
    [d.time, main_SUS_SRCL_z_out, 'b', d.time[inv], main_SUS_SRCL_z_out[inv], '.r'],
    ],
    titles = [SIG_NAME, SIG_NAME+' 1st Derivative', SIG_NAME+' 2nd Derivative', 'SRCL Offset'],
    ylabels = ['[--]', '[--]', '[--]', '$\lambda$'],
    scales = ['log', 'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
plt.xlim([0, d.time[-1]])
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- Normalized Signals -- "+SIG_NAME+" Derivatives",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
#plt.savefig(data_dir+'/DC_Trig_04_Norm_'+SIG_NAME.replace('\\','')+'_Deriv.png', dpi=300, bbox_inches='tight')


# %% Final Selection Rules
# A
trig_B4_12 = find_thr_norm(B4_12MHz_MAG_NORM, cut, 'geq', 0.7)
trig1 = cut[trig_B4_12]

# B
trig_B1p_DC = find_thr_norm(B1p_6MHz_DC_NORM, cut, 'leq', 0.1)
trig2 = cut[trig_B4_12 & trig_B1p_DC]

# C
trig_B4_112 = find_thr_norm(B4_112MHz_MAG_NORM, cut, 'geq', 0.9)
trig3 = cut[trig_B4_112]

# D
trig_B4_12 = find_thr_norm(B4_12MHz_MAG_NORM, cut, 'geq', 0.98)
trig = cut[trig_B4_12]
trig_B1p_DC = find_thr_norm(B1p_6MHz_DC_NORM, cut, 'leq', 0.001)
trig = cut[trig_B4_12 & trig_B1p_DC]
trig_B4_112 = find_thr_norm(B4_112MHz_MAG_NORM, cut, 'geq', 0.98)
trig = cut[trig_B4_12 & trig_B1p_DC & trig_B4_112]
trig_B4_DC = find_thr_norm(B4_6MHz_DC_NORM, cut, 'leq', 0.04)
trig4 = cut[trig_B4_12 & trig_B1p_DC & trig_B4_112 & trig_B4_DC]


# %% Plot Triggers
#plt.style.use('dark_background')
#plt.style.use('default')
axes = multi2plot([
    [d.time, B4_6MHz_DC_NORM, 'b', d.time[trig1], B4_6MHz_DC_NORM[trig1], '.k',
     d.time[trig2], B4_6MHz_DC_NORM[trig2], '.r', d.time[trig3], B4_6MHz_DC_NORM[trig3], '.y',
     d.time[trig4], B4_6MHz_DC_NORM[trig4], '*g'],
    [d.time, B1p_6MHz_DC_NORM, 'b', d.time[trig1], B1p_6MHz_DC_NORM[trig1], '.k',
     d.time[trig2], B1p_6MHz_DC_NORM[trig2], '.r', d.time[trig3], B1p_6MHz_DC_NORM[trig3], '.y',
     d.time[trig4], B1p_6MHz_DC_NORM[trig4], '*g'],
    [d.time, B4_12MHz_MAG_NORM, 'b', d.time[trig1], B4_12MHz_MAG_NORM[trig1], '.k',
     d.time[trig2], B4_12MHz_MAG_NORM[trig2], '.r', d.time[trig3], B4_12MHz_MAG_NORM[trig3], '.y',
     d.time[trig4], B4_12MHz_MAG_NORM[trig4], '*g'],
    [d.time, B4_112MHz_MAG_NORM, 'b', d.time[trig1], B4_112MHz_MAG_NORM[trig1], '.k',
     d.time[trig2], B4_112MHz_MAG_NORM[trig2], '.r', d.time[trig3], B4_112MHz_MAG_NORM[trig3], '.y',
     d.time[trig4], B4_112MHz_MAG_NORM[trig4], '*g'],
    [d.time, main_SUS_MICH_z_out, 'b', d.time[trig1], main_SUS_MICH_z_out[trig1], '.k',
     d.time[trig2], main_SUS_MICH_z_out[trig2], '.r', d.time[trig3], main_SUS_MICH_z_out[trig3], '.y',
     d.time[trig4], main_SUS_MICH_z_out[trig4], '*g'],
    [d.time, main_SUS_PRCL_z_out, 'b', d.time[trig1], main_SUS_PRCL_z_out[trig1], '.k',
     d.time[trig2], main_SUS_PRCL_z_out[trig2], '.r', d.time[trig3], main_SUS_PRCL_z_out[trig3], '.y',
     d.time[trig4], main_SUS_PRCL_z_out[trig4], '*g'],
    [d.time, main_SUS_SRCL_z_out, 'b', d.time[trig1], main_SUS_SRCL_z_out[trig1], '.k',
     d.time[trig2], main_SUS_SRCL_z_out[trig2], '.r', d.time[trig3], main_SUS_SRCL_z_out[trig3], '.y',
     d.time[trig4], main_SUS_SRCL_z_out[trig4], '*g'],
    ],
    titles = ['B4 DC', 'B1p DC',
              'B4 12MHz MAG', 'B4 112MHz MAG',
              'MICH Offset', 'PRCL Offset', 'SRCL Offset'],
    ylabels = ['[a.u.]', '[mA]',
               '[a.u.]', '[a.u.]',
               '$\lambda$', '$\lambda$', '$\lambda$'],
    scales = ['log', 'linear',
              'linear', 'linear',
              'linear', 'linear', 'linear'],
    xlabel = 'Time (s)')
for ax in axes:
    x = ax.get_lines()
    x[0].set_markersize(6)
    x[1].set_markersize(8)
    x[2].set_markersize(10)
    x[3].set_markersize(12)
    x[4].set_markersize(14)
plt.xlim([0, d.time[-1]])
AutoScaleY(plt.gca())
plt.grid(True, which='both', linestyle=':',alpha=0.8)
#plt.gcf().suptitle("DRMI -- DC/MAG Triggers vs. DOF Scan -- Normalized Signals -- Final Trigger ("+str(len(trig))+"/"+str(len(cut))+" pts.)",fontsize=22)
plt.gcf().suptitle("DRMI -- Inverse Trigger Selection Rules (normalized signals)",fontsize=22)
# bbox = dict(boxstyle="round,pad=0.25", fc="0.96",ec="k", lw=1.25)
# offset = 25
# axes[-1].plot(SRCL_X,SRCL_Y, 'o', color='DarkGreen', lw=2)
# for x, y in zip(SRCL_X,SRCL_Y):
#     axes[-1].annotate(f'SRCL = {y:.3f}', (x,y)
#                       , xytext=(2*offset,-offset), textcoords='offset points', bbox=bbox, ha="center",va="bottom")
plt.get_current_fig_manager().window.showMaximized()
plt.show()
plt.pause(0.1)
#plt.savefig(data_dir+'/DC_Trig_05_Final_Trigger.png', dpi=300, bbox_inches='tight')
plt.savefig('/home/blind/Downloads/test_CAR_lock2.png', dpi=300, bbox_inches='tight')
