# IFOsimWorkshop2023

Indico page with schedule: https://indico.nikhef.nl/event/4479

Group work task descriptions: [Google Slides](https://docs.google.com/presentation/d/1KXbMgbG1pX7G_qY4BlbqRjtt0aIHAkZQhOV5pmGprAE/edit?usp=sharing) 

# FINESSE Installation

On Monday afternoon we will be holding a Finesse 3 tutorial. This will be a hands-on session. Please now follow the instructions at

https://finesse.ifosim.org/docs/develop/getting_started/install/first.html

to install Finesse, or if you have already installed it, please update it to ensure you have the latest version.

You can check this by running the following command:
```
python -c "import finesse; print(finesse.__version__)"
```
which should return 3.0a8.

Note that we recommend installing via conda-forge - your conda version should also therefore be recent (e.g. version 23.x).

If you run into problems, please contact us using the Finesse Users chat channel:

https://matrix.to/#/#finesse:matrix.org

there, you can check if others have also run into similar issues.
